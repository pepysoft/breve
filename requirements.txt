 #the basic framework
starlette 
#async server
uvicorn 
#on development
pylint 
#server for high output
gunicorn 
 #template engine
yatl
# the not orm orm 
pydal 
# to serve files async
aiofiles 

