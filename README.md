# An example Starlette application adapted to use yatl as template



Install and run:

```shell
git clone https://gitlab.com/pepysoft/breve.git
cd breve
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt 
uvicorn app:app

```

Open `http://127.0.0.1:8000/` in your browser:

![Homepage](https://gitlab.com/pepysoft/breve/-/tree/master/docs/index.png)

Navigate to path that is not routed, eg `http://127.0.0.1:8000/nope`:

![404](https://gitlab.com/pepysoft/breve/-/tree/master/docs/404.png)

Raise a server error by navigating to `http://127.0.0.1:8000/error`:

![500](https://gitlab.com/pepysoft/breve/-/tree/master/docs/500.png)

Switch the `app = Starlette(debug=True)` line to `app = Starlette()` to see a regular 500 page instead.
