from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.responses import HTMLResponse

import uvicorn

#allows type anotations 
from typing import Any, Dict, List, Set

#template engine from web2py/py4web
from yatl.template import render
#templates preparations
import os,sys
#to define path to templates
pathname = os.path.dirname(sys.argv[1]) 
pathname = os.path.abspath(pathname)
path = os.path.join(pathname,'views')

def page(filename='layout.html',context= Dict[str,Any],status_code:int = 200): 
    filename = os.path.join(path,filename)
    return HTMLResponse(render(filename=filename ,path=path,context=context))


#starts the app, remove debug for production
app = Starlette(debug=True)

#mount the app
app.mount('/static', StaticFiles(directory='static'), name='static')


@app.route('/')
async def homepage(request):
    template = "index.html"
    context = {"request": request}
    return page(template, context)


@app.route('/error')
async def error(request):
    """
    An example error. Switch the `debug` setting to see either tracebacks or 500 pages.
    """
    raise RuntimeError("Oh no")


@app.exception_handler(404)
async def not_found(request, exc):
    """
    Return an HTTP 404 page.
    """
    template = "404.html"
    context = {"request": request}
    return page(template, context, status_code=404)


@app.exception_handler(500)
async def server_error(request, exc):
    """
    Return an HTTP 500 page.
    """
    template = "500.html"
    context = {"request": request}
    return page(template, context, status_code=500)


if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=8000)
